#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow)
{
	ui->setupUi(this);

	canvas = new Canvas();
	ui->grid_layout->addWidget(canvas);
}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::on_normal_cell_triggered()
{
	canvas->selected_square_type = NORMAL;
}

void MainWindow::on_wall_triggered()
{
	canvas->selected_square_type = WALL;
}

void MainWindow::on_cold_source_triggered()
{
	canvas->selected_square_type = COLD;
}

void MainWindow::on_heat_source_triggered()
{
	canvas->selected_square_type = HEAT;
}

void MainWindow::on_window_triggered()
{
	canvas->selected_square_type = WINDOW;
}

void MainWindow::on_play_triggered()
{
	canvas->set_simulation(true);
}

void MainWindow::on_pause_triggered()
{
	canvas->set_simulation(false);
}

void MainWindow::on_applyTemp_clicked()
{
	QString min_temp = ui->minTempText->toPlainText();
	QString max_temp = ui->maxTempText->toPlainText();

	if (min_temp.count() && max_temp.count())
	{
		canvas->set_min_max_temperature(min_temp.toInt(), max_temp.toInt());
		return;
	}

	if (min_temp.count())
		canvas->set_min_temperature(min_temp.toInt());
	if (max_temp.count())
		canvas->set_max_temperature(max_temp.toInt());
}



