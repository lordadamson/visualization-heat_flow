#ifndef CANVAS_H
#define CANVAS_H

#include <QWidget>
#include <QBrush>
#include <QMouseEvent>
#include <QImage>
#include <QtWidgets>
#include <QOpenGLWidget>

#include <vector>
#include <queue>

#include "square.h"

using namespace std;

class Canvas : public QOpenGLWidget
{
	Q_OBJECT

public:
	Canvas(QOpenGLWidget *parent = 0);

	QSize minimumSizeHint() const Q_DECL_OVERRIDE;
	QSize sizeHint() const Q_DECL_OVERRIDE;
	void setBrush(const QBrush &brush);

	SquareType selected_square_type = HEAT;
	void set_simulation(bool value);
	void set_min_temperature(int p_temperature);
	void set_max_temperature(int p_temperature);
	void set_min_max_temperature(int min_temperature, int max_temperature);


protected:
	void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;

private:
	QImage image;
	QBrush brush;
	const int canvas_size_x = 1800;
	const int canvas_size_y = 900;
	vector<Square*> grid;
	vector<Square*> heat_sources;
	vector<Square*> cold_sources;
	int square_size = 30;
	int start_temperature = 0;
	int end_temperature = 100;
	bool mouse_pressed = false;
	int old_index = 0;
	int xmax = 30;
	int ymax = 1770;
	queue<int> operations;
	QTimer *timer;
	bool simulating = false;
	bool simulating_mouse_click_temp = false;

	void mousePressEvent(QMouseEvent *event);
	void mouseReleaseEvent(QMouseEvent *event);
	void mouseMoveEvent(QMouseEvent *event);
	void init_grid();
	int get_index(int x, int y);
	void modify_grid_and_draw_image(int index);
	void update_square_temperature(int index, vector<Square*>* old_grid);
	vector<Square*>* copy_grid();

public slots:
	void _on_square_temperature_changed(int index);
	void update_canvas();
	void event_loop();
	void simulate();

signals:
	void square_temperature_changed(int index);
};

#endif // CANVAS_H
