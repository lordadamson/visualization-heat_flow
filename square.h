#ifndef SQUARE_H
#define SQUARE_H

#include <QColor>
#include <QRect>

#include <vector>

using namespace std;

typedef int SquareType;

static const SquareType WALL = 0;
static const SquareType NORMAL = 1;
static const SquareType WINDOW = 2;
static const SquareType HEAT = 3;
static const SquareType COLD = 4;

class Square
{
	enum SquarePosition {
		TopLeftCorner,
		TopRightCorner,
		BottomLeftCorner,
		BottomRightCorner,
		FirstRow,
		FirstColumn,
		LastRow,
		LastColumn,
		Normal
	};

	int xmax = 30;
	int ymax = 60;

	int temperature;
	int start_temperature = 0;
	int end_temperature = 100;

	void figure_out_neighbor_squares(int index);
	int different_square_cases(int index);
	void get_row_column(int index, int &row, int &column);
	QColor temperature_to_rgb(int temperature);

public:
	QRect rect;
	int size;
	int index;
	SquareType type;
	QColor color;
	vector<int> neighbor_indices;
	Square();
	Square(QRect p_rect, int p_size, int p_index, SquareType p_type, int p_temperature, QColor p_color);

	void set_temperature(int p_temperature);
	int get_temperature();
	void set_type(SquareType p_type);
	SquareType get_type();
	void increase_temperature(int value);
	void set_start_temperature(int p_temperature);
	void set_end_temperature(int p_temperature);
	void set_start_end_temperature(int p_temperature, int p_tempreature);
};

#endif // SQUARE_H
