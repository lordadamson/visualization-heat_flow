#include "square.h"
#include <iostream>

Square::Square()
{

}

Square::Square(QRect p_rect, int p_size, int p_index, SquareType p_type, int p_temperature, QColor p_color)
{
	rect = p_rect;
	size = p_size;
	index = p_index;
	type = p_type;
	temperature = p_temperature;
	color = p_color;
	figure_out_neighbor_squares(index);
}

void Square::set_temperature(int p_temperature)
{
	if (type != NORMAL)
		return;
	if (temperature > end_temperature)
		temperature = end_temperature;
	if (temperature < start_temperature)
		temperature = start_temperature;
	temperature = p_temperature;
	color = temperature_to_rgb(temperature);
}

int Square::get_temperature()
{
	return temperature;
}

void Square::set_type(SquareType p_type)
{
	type = p_type;
	color = type;
	if (type == WALL)
		color == Qt::black;
	if (type == WINDOW)
	{
		temperature = (end_temperature+start_temperature)/2;
		color = Qt::green;
	}
	if (type == HEAT)
	{
		temperature = end_temperature;
		color = Qt::red;
	}
	if (type == COLD)
	{
		temperature = start_temperature;
		color = Qt::blue;
	}
	if (type == NORMAL)
	{
		temperature = (end_temperature+start_temperature)/2;
		color = Qt::green;
	}

}

SquareType Square::get_type()
{
	return type;
}

void Square::increase_temperature(int value)
{
	if (type != NORMAL)
		return;
	temperature += value;
	if (temperature > end_temperature)
		temperature = end_temperature;
	color = temperature_to_rgb(temperature);
}

void Square::set_start_temperature(int p_temperature)
{
	start_temperature = p_temperature;
}

void Square::set_end_temperature(int p_temperature)
{
	end_temperature = p_temperature;
}

void Square::set_start_end_temperature(int min_temp, int max_temp)
{
	start_temperature = min_temp;
	end_temperature = max_temp;
}

QColor Square::temperature_to_rgb(int temperature)
{
	/*
	 *		   R	  G	   B
	 * 0   -> (0,     0, 255) from 0 to 25 the B stays constant and the G increases
	 * 25  -> (0,   255, 255) from 25 to 50 the G stays constant and the B decreases
	 * 50  -> (0,   255,   0) from 50 to 75 the G stays constant and the R increases
	 * 75  -> (255, 255,   0) from 75 to 100 the R stays constant and the G decreases
	 * 100 -> (255,   0,   0)
	 */
	if (temperature > end_temperature)
		temperature = end_temperature;
	if (temperature < start_temperature)
		temperature = start_temperature;

	double temp = (double)temperature;
	double end_temp = (double)end_temperature;

	if (temp < end_temp/4)
		return QColor(0, (temp/(end_temp/4)*255), 255);
	if (temp >= end_temp/4 && temp < end_temp/2)
		return QColor(0, 255, (1 - ((temp - (end_temp/4))/(end_temp/4))) * 255);
	if (temp >= end_temp/2 && temp < 3*end_temp/4)
		return QColor( (temp - (end_temp/2)) / ( end_temp/4 ) * 255, 255, 0);
	if (temp <= end_temp)
		return QColor(255, (1 - ((temp - (3*end_temp/4)) / (end_temp/4))) * 255, 0);
}

int Square::different_square_cases(int index)
{
	int row, column;
	get_row_column(index, row, column);

	if (column == 0 && row == 0)
		return SquarePosition::TopLeftCorner;
	if (column == 0 && row == xmax - 1)
		return SquarePosition::BottomLeftCorner;
	if (column == ymax - 1 && row == 0)
		return SquarePosition::TopRightCorner;
	if (column == ymax - 1 && row == xmax - 1)
		return SquarePosition::BottomRightCorner;
	if (column == 0)
		return SquarePosition::FirstColumn;
	if (column == ymax - 1)
		return SquarePosition::LastColumn;
	if (row == 0)
		return SquarePosition::FirstRow;
	if (row == xmax - 1)
		return SquarePosition::LastRow;
	return SquarePosition::Normal;
}

void Square::get_row_column(int index, int &row, int &column)
{
	column = (int)((float)(index)/(float)xmax);
	row = index - column*xmax;
}

void Square::figure_out_neighbor_squares(int index)
{
	int my_neighbors[] = {
		index - (int)xmax - 1,  index - 1,  index + (int)xmax - 1,
		index - (int)xmax,		index,		index + (int)xmax,
		index - (int)xmax + 1,  index + 1,  index + (int)xmax + 1
	};

	SquareType condition = different_square_cases(index);

	if (condition == SquarePosition::Normal)
		neighbor_indices = vector<int>(my_neighbors, my_neighbors+9);
	if (condition == SquarePosition::TopLeftCorner)
		neighbor_indices = {my_neighbors[4], my_neighbors[5], my_neighbors[7], my_neighbors[8]};
	if (condition == SquarePosition::BottomLeftCorner)
		neighbor_indices = {my_neighbors[1], my_neighbors[2], my_neighbors[4], my_neighbors[5]};
	if (condition == SquarePosition::TopRightCorner)
		neighbor_indices = {my_neighbors[3], my_neighbors[4], my_neighbors[6], my_neighbors[7]};
	if (condition == SquarePosition::BottomRightCorner)
		neighbor_indices = {my_neighbors[0], my_neighbors[1], my_neighbors[3], my_neighbors[4]};
	if (condition == SquarePosition::FirstColumn)
		neighbor_indices = {my_neighbors[1], my_neighbors[2], my_neighbors[4], my_neighbors[5], my_neighbors[7], my_neighbors[8]};
	if (condition == SquarePosition::LastColumn)
		neighbor_indices = {my_neighbors[0], my_neighbors[1], my_neighbors[3], my_neighbors[4], my_neighbors[6], my_neighbors[7]};
	if (condition == SquarePosition::FirstRow)
		neighbor_indices = {my_neighbors[3], my_neighbors[4], my_neighbors[5], my_neighbors[6], my_neighbors[7], my_neighbors[8]};
	if (condition == SquarePosition::LastRow)
		neighbor_indices = {my_neighbors[0], my_neighbors[1], my_neighbors[2], my_neighbors[3], my_neighbors[4], my_neighbors[5]};
}
