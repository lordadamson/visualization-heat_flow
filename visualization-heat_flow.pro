#-------------------------------------------------
#
# Project created by QtCreator 2017-01-01T19:06:47
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets openglwidgets

TARGET = visualization-heat_flow
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    canvas.cpp \
    square.cpp

HEADERS  += mainwindow.h \
    square.h \
    canvas.h

FORMS    += mainwindow.ui

DISTFILES += \
    assets/green.png \
    assets/window.png \
    assets/red.png \
    assets/play.png \
    assets/pause.png \
    assets/blue.png \
    assets/black.png
