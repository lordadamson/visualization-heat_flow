#include "canvas.h"

#include <QPainter>
#include <QImage>
#include <QtMath>
#include <iostream>
#include <thread>

using namespace std;

Canvas::Canvas(QOpenGLWidget *parent) : QOpenGLWidget(parent)
{
	setBackgroundRole(QPalette::Base);
	setAutoFillBackground(true);
	setFixedSize(canvas_size_x, canvas_size_y);

	init_grid();

	image = QImage(width(), height(), QImage::Format_RGB888);
	QPainter painter(&image);
	painter.setBrush(Qt::green);
	painter.setPen(Qt::NoPen);

	for (Square* square : grid)  // <-- return a reference on each element.
		painter.drawRect(square->rect);

	//connect(this, &Canvas::square_temperature_changed, this, &Canvas::_on_square_temperature_changed);
	timer = new QTimer(this);
	connect(timer, SIGNAL(timeout()), this, SLOT(simulate()));
	timer->start(100);
}

void Canvas::init_grid()
{
	// initialize grid and fill with squares
	int index = 0;
	for (int i = 0; i < width(); i += square_size)
		for(int j = 0; j < height(); j += square_size, index++)
			grid.push_back(
				new Square(QRect(i, j, square_size, square_size), square_size, index, NORMAL,
					   (end_temperature - start_temperature)/2,
					   Qt::green)
						  );
}

QSize Canvas::minimumSizeHint() const
{
	return QSize(100, 100);
}

QSize Canvas::sizeHint() const
{
	return QSize(400, 200);
}

void Canvas::setBrush(const QBrush &brush)
{
	this->brush = brush;
	update();
}

void Canvas::set_simulation(bool value)
{
	simulating = value;
	if(simulating)
		simulate();
}

void Canvas::set_min_temperature(int p_temperature)
{
	start_temperature = p_temperature;
	for (Square* square : grid)
		square->set_start_temperature(start_temperature);
}

void Canvas::set_max_temperature(int p_temperature)
{
	end_temperature = p_temperature;
	for (Square* square : grid)
		square->set_end_temperature(end_temperature);
}

void Canvas::set_min_max_temperature(int min_temperature, int max_temperature)
{
	start_temperature = min_temperature;
	end_temperature = max_temperature;
	for (Square* square : grid)
		square->set_start_end_temperature(min_temperature, max_temperature);
}

// is called every screen update
void Canvas::paintEvent(QPaintEvent * event)
{
	QPainter painter(this);
	QRect event_rect = event->rect();
	painter.drawPixmap(event_rect, QPixmap::fromImage(image));
}

// returns the index of the square on which the cursor stands
int Canvas::get_index(int x, int y)
{
	int index = 0;
	x = x / square_size;
	y = y / square_size;
	index = square_size*x + y;
	return index;
}

void Canvas::modify_grid_and_draw_image(int index)
{
	if (index >= grid.size())
		return;

	grid[index]->set_type(selected_square_type);
	if (selected_square_type == HEAT)
		heat_sources.push_back(grid[index]);
	if (selected_square_type == COLD)
		cold_sources.push_back(grid[index]);
	//operations.push(index);

//	if (selected_wall_type == WALL)
//		return;
	QPainter painter(&image);
	QColor color = grid[index]->color;
	//cout << color.red() << " " << color.green() << " " << color.blue() << endl;
	painter.setBrush(grid[index]->color);
	painter.setPen(Qt::NoPen);
	painter.drawRect(grid[index]->rect);
	update();
	//emit square_temperature_changed(index);
}

vector<Square *>* Canvas::copy_grid()
{
	vector<Square*>* new_grid = new vector<Square*>();

	for (Square* square : grid)
		new_grid->push_back(square);

	return new_grid;
}

void Canvas::update_square_temperature(int index, vector<Square*>* old_grid)
{
	vector<int> neighbors = old_grid->at(index)->neighbor_indices;
	int average_temperature, temperature_sum = 0;

	int counter = 0;

	for (int neighbor : neighbors)
	{
		if (old_grid->at(neighbor)->get_type() == WALL)
			continue;
		if (old_grid->at(index)->get_temperature() > 50)
			temperature_sum++;
		temperature_sum += old_grid->at(neighbor)->get_temperature();
		counter++;
	}

	average_temperature = temperature_sum/counter;

	grid[index]->set_temperature(average_temperature);
//	operations.push(index);

//	if (qAbs(old_temperature - average_temperature) >= 2)
//		emit square_temperature_changed(index);
}



void Canvas::simulate()
{
	for (Square* square : grid)
	{
		if(!simulating)
			break;

		vector<Square*>* old_grid = copy_grid();
		update_square_temperature(square->index, old_grid);

		QPainter painter(&image);
		painter.setBrush(square->color);
		painter.setPen(Qt::NoPen);
		painter.drawRect(square->rect);
		update();
	}
}

void Canvas::event_loop()
{
	while(true)
		update_canvas();
}

void Canvas::update_canvas()
{
	if (operations.empty())
		return;
	int index = operations.front();
	operations.pop();
	QPainter painter(&image);
	QColor color = grid[index]->color;
	//cout << color.red() << " " << color.green() << " " << color.blue() << endl;
	painter.setBrush(grid[index]->color);
	painter.setPen(Qt::NoPen);
	painter.drawRect(grid[index]->rect);
	update();
}

void Canvas::_on_square_temperature_changed(int index)
{
	vector<int> neighbors = grid[index]->neighbor_indices;

//	for (int neighbor : neighbors)
//		update_square_temperature(neighbor);
}

void Canvas::mousePressEvent(QMouseEvent *event)
{
	simulating_mouse_click_temp = simulating;
	set_simulation(false);
//	if(simulating)
//		return;
	mouse_pressed = true;
	int index = get_index(event->x(), event->y());
	old_index = index;
	modify_grid_and_draw_image(index);
}

void Canvas::mouseMoveEvent(QMouseEvent *event)
{
	if(simulating)
		return;
	int index = get_index(event->x(), event->y());
	if (old_index == index)
		return;
	modify_grid_and_draw_image(index);
}

void Canvas::mouseReleaseEvent(QMouseEvent *event)
{
//	if(simulating)
//		return;
	int index = get_index(event->x(), event->y());
	old_index = index;
	modify_grid_and_draw_image(index);
	mouse_pressed = false;
	set_simulation(simulating_mouse_click_temp);
}

